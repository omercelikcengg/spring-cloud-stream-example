package tr.cloud.stream.examples.configuration;

import org.springframework.cloud.stream.annotation.EnableBinding;
import tr.cloud.stream.examples.stream.PersonStream;

@EnableBinding({ PersonStream.class })
public class StreamConfiguration {
}
