package tr.cloud.stream.examples.stream;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface PersonStream {
    String INPUT = "person-topic-in";

    @Input(INPUT)
    SubscribableChannel inboundPerson();
}
