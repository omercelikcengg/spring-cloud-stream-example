
# Occuring Situation : 
Data came to streamlistener 5 times in 1 second.
I set Concurrency to 3. There was no change in the situation.
I want to receive data periodically.
And there is always a lot of data on the topic.
But I don't have enough resources.
I want to pull data in bulk every 5 seconds.
However, I could never do that.
It comes to streamlistener as much as the number of partitions in 5 seconds. I want it to come to streamlistener only once in 5 seconds.

# I made two separate projects, producer and consumer. Run both of them. Then follow the consumers' logs.

# Current Situtaion : 

Count : 650 Date : Thu Mar 03 12:21:50 EET 2022

Count : 1000 Date : Thu Mar 03 12:21:50 EET 2022

Count : 700 Date : Thu Mar 03 12:21:50 EET 2022



Count : 800 Date : Thu Mar 03 12:21:55 EET 2022

Count : 900 Date : Thu Mar 03 12:21:55 EET 2022

Count : 720 Date : Thu Mar 03 12:21:55 EET 2022




# Expected Situtation : 

Count : 1000 Date : Thu Mar 03 12:21:50 EET 2022

Count : 1000 Date : Thu Mar 03 12:21:55 EET 2022