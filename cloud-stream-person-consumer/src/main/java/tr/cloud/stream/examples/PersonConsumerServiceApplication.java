package tr.cloud.stream.examples;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PersonConsumerServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(PersonConsumerServiceApplication.class, args);
    }
}
