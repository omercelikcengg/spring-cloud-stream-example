package tr.cloud.stream.examples.stream;

import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Service;
import tr.cloud.stream.examples.model.Person;

import java.util.Date;
import java.util.List;

@Service("personListenerImpl")
public class PersonListenerImpl {

    @StreamListener(value = PersonStream.INPUT)
    public void personReceiverWithOutCondition(List<Person> personList) {
        System.out.println("Count :  " + personList.size() + " Date : " + new Date(System.currentTimeMillis()));
    }

}


