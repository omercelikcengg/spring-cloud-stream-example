package tr.cloud.stream.examples.model;

import java.io.Serializable;

public class Person implements Serializable {

    private String name;
    private String surname;
    private Long salary;

    public Person() {
    }

    public Person(String name, String surname, Long salary) {
        this.name = name;
        this.surname = surname;
        this.salary = salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Long getSalary() {
        return salary;
    }

    public void setSalary(Long salary) {
        this.salary = salary;
    }
}
