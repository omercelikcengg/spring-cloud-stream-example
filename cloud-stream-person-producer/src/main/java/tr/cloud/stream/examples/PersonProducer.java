package tr.cloud.stream.examples;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.SmartLifecycle;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import tr.cloud.stream.examples.model.Person;
import tr.cloud.stream.examples.stream.PersonStream;

import java.util.Random;

@Service
public class PersonProducer implements SmartLifecycle {

    @Autowired
    private PersonStream personStream;

    private ObjectMapper objectMapper = new ObjectMapper();

    private static int count = 0;

    @Override
    public void start() {
        while (true) {
            count++;
            waitSpecificSecond();
            try {
                int key = new Random().nextInt(15);
                Person person = new Person("Omer", "Celik", (long) key);

                MessageChannel messageChannel = personStream.outboundPerson();

                byte[] byteKey = objectMapper.writeValueAsBytes(key);
                messageChannel.send(MessageBuilder.withPayload(person)
                        .setHeader(KafkaHeaders.MESSAGE_KEY, byteKey)
                        .build());
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
    }

    public void waitSpecificSecond() {
        if (count == 3000) {
            try {
                Thread.sleep(3000);
                count = 0;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void stop() {

    }

    @Override
    public boolean isRunning() {
        return false;
    }

    @Override
    public int getPhase() {
        return Integer.MAX_VALUE;
    }
}
