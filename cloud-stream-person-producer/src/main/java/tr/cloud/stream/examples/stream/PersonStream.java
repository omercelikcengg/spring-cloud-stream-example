package tr.cloud.stream.examples.stream;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface PersonStream {
    String OUTPUT = "person-topic-out";

    @Output(OUTPUT)
    MessageChannel outboundPerson();
}
